#include<iostream>
using namespace std;
class student
{
    public:
    string fullName;
    int rollNum;
    double semPerentage;
   string collegeName;
   int collegeCode;
   student(string fn,int r,double sp,string cn,int cc)
   {
       fullName = fn;
       rollNum = r;
       semPerentage = sp;
       collegeName= cn;
       collegeCode= cc;
   }
   void display()
   {
       cout<< fullName<<" "<<rollNum<<" "<<semPerentage<<" "<<collegeName<<" "<<collegeCode<<endl;
   }
   ~student()
   {
       cout<<"obj is destroyed"<<endl;
   }
};
int main()
{
    student obj1("Hari",1,70.00,"mvgr",33);
    obj1.display();
    student obj2("Kiran",2,73.00,"mvgr",33);
    obj2.display();
    return 0;
}