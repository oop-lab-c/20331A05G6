import java.util.*;

class UserExHandJava 
{
	public static void main(String[] args)
	{
		try {
			System.out.println("inside try block");
	
			System.out.println(95 / 5);
		}
	
		catch (ArithmeticException e) {
			
			System.out.println("Arithmetic Exception");
			
		}
	
		finally {
			
			System.out.println(
				"\nfinished");
			
		}
	}
}