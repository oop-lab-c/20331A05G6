class animal
{
    void bark()
    {
        System.out.println("Dog bark like  ");
    }
}
class dog extends animal
{
    void bow()
    {
        System.out.println("bow..bow..");
    }
    public static void main(String[] args)
    {
        dog d = new dog();
        d.bark();
        d.bow();
    }
}