#include<iostream>
using namespace std;
class AccessSpecifierDemo
{
    private:
    int priVar;
    protected:
    int proVar;
    public:
    int pubVar;
    public:
    void setVar(int priValue,int proValue, int pubValue)
    {
        priVar=priValue;
        proVar=proValue;
        pubVar=pubValue;
    }
    void getVar()
    {
        cout<<"the value of privar is "<<priVar<<endl;
        cout<<"the value of provar is "<<proVar<<endl;
        cout<<"the value of pubvar is "<<pubVar<<endl;
    }
};
int main()
{
    AccessSpecifierDemo obj;
    obj.setVar(12,23,34);
    obj.getVar();
    return 0;
}